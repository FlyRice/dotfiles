" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages.
runtime! archlinux.vim

" If you prefer the old-style vim functionalty, add 'runtime! vimrc_example.vim'
" Or better yet, read /usr/share/vim/vim80/vimrc_example.vim or the vim manual
" and configure vim to your own liking!

set number
set autoindent
"colo koehler
syntax on

" special tab settings
set tabstop=4		" tab size = 4
set shiftwidth=4	" width for autoindents
set expandtab 		" replace tabs with spaces
set softtabstop=4 	" backspace as many spaces as would be a tab's length
set wildmode=longest,list   " shell-like tab completions

" Allows loading of language-specific indentation (files found in ~/.vim/indent/*.vim)
filetype plugin indent on

" highlight matching {[()]}
set showmatch

" enable mouse click
set mouse=a

" use system clipboard
set clipboard=unnamedplus

" backup directory
set backupdir=~/.cache/vim

" how vim should split panes (right &/ below)
set splitright
set splitbelow

"""""""""""""""""
"   keybindings "
"""""""""""""""""
" move split panes to left/bottom/top/right
 nnoremap <A-h> <C-W>H
 nnoremap <A-j> <C-W>J
 nnoremap <A-k> <C-W>K
 nnoremap <A-l> <C-W>L
" move between panes to left/bottom/top/right
 nnoremap <C-h> <C-w>h
 nnoremap <C-j> <C-w>j
 nnoremap <C-k> <C-w>k
 nnoremap <C-l> <C-w>l

 " copies filepath to clipboard by pressing yf
:nnoremap <silent> yf :let @+=expand('%:p')<CR>
" copies pwd to clipboard: command yd
:nnoremap <silent> yd :let @+=expand('%:p:h')<CR>
" Vim jump to the last position when reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif
