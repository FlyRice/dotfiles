#!/usr/bin/env bash

killall -q polybar
polybar example1 2>&1 | tee -a /tmp/polybar.log & disown
polybar example2 2>&1 | tee -a /tmp/polybar.log & disown
echo "Polybar launched..."
