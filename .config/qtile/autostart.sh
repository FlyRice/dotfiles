#!/usr/bin/env bash

exec $HOME/.screenlayout/ultrawide.sh &
$HOME/.config/polybar/launch.sh
nitrogen --restore &
sxhkd -c $HOME/.config/sxhkd/sxhkdrc &
picom &

# extra bollocks from arco's config
nm-applet &
pamac-tray &
xfce4-power-manager &
volumeicon &
/usr/lib/polkit-1/polkitd --no-debug &
#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
/usr/lib/xfce4/notifyd/xfce4-notifyd &
