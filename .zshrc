# Use powerline
USE_POWERLINE="true"
# Source manjaro-zsh-configuration
if [[ -e /usr/share/zsh/manjaro-zsh-config ]]; then
  source /usr/share/zsh/manjaro-zsh-config
fi
# Use manjaro zsh prompt
if [[ -e /usr/share/zsh/manjaro-zsh-prompt ]]; then
  source /usr/share/zsh/manjaro-zsh-prompt
fi

# Default programs
export EDITOR=nvim
export BROWSER=firefox

# Enable colors
autoload -U colors && colors

# History management
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.zhistory
setopt extended_history
setopt hist_expire_dups_first
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_find_no_dups
setopt inc_append_history

autoload -U add-zsh-hook

# Basic auto/tab complete
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)       # Include hidden files

# vim mode
#bindkey -v
#export KEYTIMEOUT=1

# TODO: Use ranger to switch directories and bind it to ctrl-O
#rangercd() {

# Edit line in vim with ctrl-E
#autoload edit-command-line; zle -N edit-command-line
#bindkey '*e' edit-command-line

# zsh options (shopt in bash)
setopt globdots
setopt extendedglob
setopt auto_list

# Ensuring PATH variable
export PATH="$HOME/bin:$PATH"

# aliases 
alias ls='exa --color=auto'
alias grep='grep --colour=auto'
alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias ll='ls -lh --icons --group-directories-first -F'
alias la='ls -a'
alias cls='clear; pwd; ls'
alias clip='xclip -selection clipboard'
alias dgit='/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME'
alias dgit-show-untracked-files='dgit config --local status.showUntrackedFiles no'
alias ytmusic='yt-dlp --audio-format mp3 --embed-thumbnail -x'
alias tclock='tty-clock -cbt'
alias rmdeps='sudo pacman -Rns $(pacman -Qtdq)'
alias xmerge='xrdb -merge ~/.Xresources'
alias debug_qtile='bat $HOME/.local/share/qtile/qtile.log'
alias today="nl $(ls $HOME/Desktop/Schedule/Schedule-* | head -n1)"
alias finish="source /opt/finish.sh"
alias nvi="neovide"

# Autostart on new shell
pfetch; cal -n2 
nl "$(/bin/ls ~/Desktop/Schedule/Schedule-* | head -n1)"
