#!/bin/bash
# changed the CONFIG array to use the cut command because my workspace names uses colons (:)

DISPLAY_CONFIG=($(i3-msg -t get_outputs | jq -r '.[]|"\(.name):\(.current_workspace)"'))

for ROW in "${DISPLAY_CONFIG[@]}"
do
	#IFS=':'
	#read -ra CONFIG <<< "${ROW}"
	if [ "$(echo $ROW | cut -d: -f1)" != "null" ] && [ "$(echo $ROW | cut -d: -f2)" != "null" ]; then
		echo "moving $(echo $ROW | cut -d: -f2-) right..."
		i3-msg -- workspace --no-auto-back-and-forth "$(echo $ROW | cut -d: -f2-)"
		i3-msg -- move workspace to output up
	fi
done
